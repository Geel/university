//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////

var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");
var libraryversion = Argument("libraryversion", "1.0.0.0");

//////////////////////////////////////////////////////////////////////
// PREPARATION
//////////////////////////////////////////////////////////////////////

// Define paths
var projectDir = Directory("./");
var buildDir = projectDir + Directory("bin/") + Directory(configuration);
var solutionFile = File("./BlaBlaPeople.sln");
var testsSolutionFile = File("./BBP_UnitTests/BBP_UnitTests.csproj");

//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////

Task("Clean")
    .Does(() =>
{
    CleanDirectory(buildDir);
});

Task("Build")
    .Does(() =>
{
    NuGetRestore(solutionFile);

    MSBuild(solutionFile, c => {
        c.Configuration = configuration;
        c.MSBuildPlatform = Cake.Common.Tools.MSBuild.MSBuildPlatform.x86;
    });
});

Task("Tests")
    .IsDependentOn("Build")
    .Does(() =>
{
    NuGetRestore(testsSolutionFile, new NuGetRestoreSettings {
        NonInteractive = true,
        PackagesDirectory = Directory("./packages")
    });
    
    MSTest("./*Tests/bin/" + configuration + "/*Tests.dll");
});

//////////////////////////////////////////////////////////////////////
// TASK TARGETS
//////////////////////////////////////////////////////////////////////

Task("Default")
    .IsDependentOn("Build")
    .IsDependentOn("Tests");

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(target);