﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BlaBlaPeople
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginScreen : ContentPage
    {
        public LoginScreen()
        {
            InitializeComponent();
        }

        private void button_Login_Click(object sender, EventArgs e)
        {
            RegexMethod regex = new RegexMethod();

            bool is_email = false;
            bool is_phone_number = false;
            bool is_username = false;

            if (username_login.Text != null && password_login != null)
            {
                is_email = regex.isEmail(username_login.Text);
                is_phone_number = regex.isPhoneNumber(username_login.Text);
            }
            else if (username_login.Text == null && password_login == null)
            {
                username_login.TextColor = Color.FromHex("C8321E");
                password_login.TextColor = Color.FromHex("C8321E");
                Invalid_data_label.Text = "Please enter login and password!";
                Invalid_data_label.IsVisible = true;
                return;
            }
            else if (username_login.Text == null)
            {
                username_login.TextColor = Color.FromHex("C8321E");
                password_login.TextColor = Color.FromHex("C8321E");
                Invalid_data_label.Text = "Please enter login";
                Invalid_data_label.IsVisible = true;
                return;
            }
            else if (password_login.Text == null)
            {
                username_login.TextColor = Color.FromHex("C8321E");
                password_login.TextColor = Color.FromHex("C8321E");
                Invalid_data_label.Text = "Please enter password";
                Invalid_data_label.IsVisible = true;
                return;
            }

            if ((!is_email && !is_phone_number) && (username_login.Text != null && password_login != null))
            {
                for (int i = 0; i < username_login.Text.Length; i++)
                {
                    if (username_login.Text[i] == '@')
                    {
                        username_login.TextColor = Color.FromHex("C8321E");
                        password_login.TextColor = Color.FromHex("C8321E");
                        Invalid_data_label.Text = "Invalid email format!";
                        Invalid_data_label.IsVisible = true;
                        return;
                    }
                    else if (char.IsLetter(username_login.Text[i]))
                    {
                        is_username = true;
                    }
                    else
                    {
                        username_login.TextColor = Color.FromHex("C8321E");
                        password_login.TextColor = Color.FromHex("C8321E");
                        Invalid_data_label.Text = "Invalid phone format!";
                        Invalid_data_label.IsVisible = true;
                        return;
                    }
                }
            }

            
            // + Data verification from server
            //...

            if (is_email || is_phone_number || is_username) // <-- test variables
            {
                App.Current.MainPage = new MainPage();
            }
            else
            {
                username_login.TextColor = Color.FromHex("C8321E");
                password_login.TextColor = Color.FromHex("C8321E");
                Invalid_data_label.Text = "Invalid username or password!";
                Invalid_data_label.IsVisible = true;
            }
        }
       
        private void login_TextChanged(object sender, TextChangedEventArgs e)
        {
            username_login.TextColor = Color.FromHex("47477F");
            password_login.TextColor = Color.FromHex("47477F");
            Invalid_data_label.IsVisible = false;
            Invalid_data_label.Text = "Invalid username or password!";
        }

        private void button_GoToRegistration_Click(object sender, EventArgs e)
        {
            App.Current.MainPage = new RegistrationScreen();
        }

        private void Button_ShowPassword_Click(object sender, EventArgs e)
        {
            password_login.IsPassword = !password_login.IsPassword;
        }
    }
}