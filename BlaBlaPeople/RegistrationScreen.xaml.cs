﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BlaBlaPeople
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistrationScreen : ContentPage
    {
        public RegistrationScreen()
        {
            InitializeComponent();
        }

        private void button_Register_Click(object sender, EventArgs e)
        {
            RegexMethod regex = new RegexMethod();

            bool is_email = regex.isEmail(username_registration.Text);
            bool is_phone_number = regex.isPhoneNumber(username_registration.Text);

        }

        private void registration_TextChanged(object sender, EventArgs e)
        {

        }

        private void button_GoToLogin_Click(object sender, EventArgs e)
        {
            App.Current.MainPage = new LoginScreen();
        }

        private void Button_ShowPassword_Click(object sender, EventArgs e)
        {
            password_registration.IsPassword = !password_registration.IsPassword;
            password_confirm_registration.IsPassword = !password_confirm_registration.IsPassword;
        }
    }
}