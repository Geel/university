﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace BlaBlaPeople
{
    public class RegexMethod
    {
        public bool isEmail(string value)
        {
            string pattern0 = @"^([A-Z|a-z|0-9](\.|_){0,1})+[A-Z|a-z|0-9]\@([A-Z|a-z|0-9])+((\.){0,1}[A-Z|a-z|0-9]){2}\.[a-z]{2,3}$";
            string pattern = @"(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" + @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))";

            if (Regex.IsMatch(value, pattern0, RegexOptions.IgnoreCase))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Positive:
        //    +42 555.123.4567
        //    +1-(800)-123-4567
        //    +7 555 1234567
        //    +7(926)1234567
        //    (926) 1234567
        //    +79261234567
        //    926 1234567
        //    9261234567
        //    1234567
        //    123-4567
        //    123-89-01
        //    495 1234567
        //    469 123 45 67
        //    89261234567
        //    8 (926) 1234567
        //    926.123.4567
        //    415-555-1234
        //    650-555-2345
        //    (416)555-3456
        //    202 555 4567
        //    4035555678
        //    1 416 555 9292
        //    111-------22
        //    12345
        //
        //Negative:
        //    926 3 4
        //    8 800 600-APPLE
        //    353_424_2424
        //    1234
        public bool isPhoneNumber(string value)
        {
            string pattern = @"^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$";

            if (Regex.IsMatch(value, pattern, RegexOptions.IgnoreCase))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}