﻿using BlaBlaPeople.Interfaces;
using BlaBlaPeople.Models.Map;
using BlaBlaPeople.Utils.Map;
using System;
using System.Threading;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BlaBlaPeople
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapScreen : ContentPage
    {
        private readonly IMapReturnData returnDataObj;

        public delegate void BackButtonClicked();
        public readonly State currentState;

        public enum State
        {
            ShowAllPins,
            GetCoords
        }

        // default constructor for initialisaton from XAML
        public MapScreen()
        {
            InitializeComponent();

            BackButton.IsVisible = false;
            currentState = State.ShowAllPins;
            map.currentMapState = currentState;

            addAllPinsOnMap();
        }

        public MapScreen(IMapReturnData obj = null, State state = State.ShowAllPins)
        {
            InitializeComponent();

            currentState = state;
            map.currentMapState = currentState;

            switch (currentState)
            {
                case State.ShowAllPins:
                    BackButton.IsVisible = false;
                    addAllPinsOnMap();
                    break;
                case State.GetCoords:
                    BackButton.IsVisible = true;
                    returnDataObj = obj;
                    break;
            }
        }
        async private void backButtonClicked(object sender, EventArgs e)
        {
            returnDataObj?.selectedPosition(map.currentPosition);
            await Navigation.PopModalAsync();
        }

        private void addAllPinsOnMap()
        {
            new Thread(async () =>
            {
                // Test data begin
                //await MapPinHelper.removeAll();
                //await MapPinHelper.store(new MapPin
                //{
                //    Title = "Lviv opera",
                //    Description = "description description description description description description description description description",
                //    ImageUrl = "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fsearchengineland.com%2Ffigz%2Fwp-content%2Fseloads%2F2015%2F12%2Fgoogle-amp-fast-speed-travel-ss-1920.jpg",
                //    Latitude = 49.845041556470065,
                //    Longitude = 24.026151685565814,
                //    PersonUsername = "username"
                //});
                // Test data end
                var pins = await MapPinHelper.getAll();
                if (pins.Count > 0)
                {
                    MainThread.BeginInvokeOnMainThread(() =>
                    {
                        foreach (var pin in pins)
                        {
                            map.addPin(pin);
                        }
                    });
                }
            }).Start();
        }
    }
}
