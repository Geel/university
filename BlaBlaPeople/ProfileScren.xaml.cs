﻿using BlaBlaPeople.Interfaces;
using BlaBlaPeople.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BlaBlaPeople
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfileScren : ContentPage
    {
        public ProfileScren()
        {
            InitializeComponent();
            username_profile.Text = User.current.Username;
            birthday_profile.Text = User.current.Birthday;
            email_profile.Text = User.current.Email;
        }

        private void Button_Update_my_profile_Click(object sender, EventArgs e)
        {
            save_button.IsVisible = !save_button.IsVisible;
            change_img.IsVisible = !change_img.IsVisible;
            username_profile.IsReadOnly = !username_profile.IsReadOnly;
            birthday_profile.IsReadOnly = !birthday_profile.IsReadOnly;
            email_profile.IsReadOnly = !email_profile.IsReadOnly;
            //await FirebaseHelper.AddFunc<User>("Users", User.current);
        }

        private void save_Clicked(object sender, EventArgs e)
        {
            var Username = User.current.Username;
            User.current.Username = username_profile.Text;
            User.current.Birthday = birthday_profile.Text;
            User.current.Email = email_profile.Text;
            User.Update_user(Username, User.current.Username, User.current.Birthday, User.current.Email);
        }

        private async void change_img_Clicked(object sender, EventArgs e)
        {
            (sender as Button).IsEnabled = false;

            Stream stream = await DependencyService.Get<IPhotoPickerService>().GetImageStreamAsync();
            if (stream != null)
            {
                profile_image.Source = ImageSource.FromStream(() => stream);
            }

            (sender as Button).IsEnabled = true;
        }
    }
}