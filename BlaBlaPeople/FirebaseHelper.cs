﻿using System;
using System.Linq;
using Firebase.Database;
using System.Diagnostics;
using System.Threading.Tasks;
using Firebase.Database.Query;
using System.Collections.Generic;

namespace BlaBlaPeople
{
    public class FirebaseHelper
    {
        //Connect app with firebase using API Url  
        public static FirebaseClient firebase = new FirebaseClient("https://xamarinfirebase-92590-default-rtdb.firebaseio.com/");

        //Get All
        public static async Task<List<T>> GetAllFunc<T>(string Table_name)
        {
            try
            {
                var list_ = (await firebase
                .Child(Table_name)
                .OnceAsync<T>())
                .Select(item => item.Object).ToList();
                return list_;
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Error:{e}");
                return new List<T>();
            }
        }

        //Add
        public static async Task<bool> AddFunc<T>(string Table_name, T object_firebase)
        {
            try
            {
                await firebase
                .Child(Table_name)
                .PostAsync(object_firebase);
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Error:{e}");
                return false;
            }
        }

        //Update     
        public static async Task<bool> UpdateFunc<T>(string Table_name, T old_obj, T updated)
        {
            try
            {
                var toUpdate = (await firebase
                .Child(Table_name)
                .OnceAsync<T>()).Where(a => a.Object.Equals(old_obj)).FirstOrDefault();
                await firebase
                .Child(Table_name)
                .Child(toUpdate.Key)
                .PutAsync(updated);
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Error:{e}");
                return false;
            }
        }

        //Delete  
        public static async Task<bool> DeleteFunc<T>(string Table_name, T to_delete)
        {
            try
            {
                var toDelete = (await firebase
                .Child(Table_name)
                .OnceAsync<T>())
                .Where(a => a.Object.Equals(to_delete)).FirstOrDefault();
                await firebase
                .Child(Table_name)
                .Child(toDelete.Key)
                .DeleteAsync();
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Error:{e}");
                return false;
            }
        }
    }
}
