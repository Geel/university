﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Maps;

namespace BlaBlaPeople.Interfaces
{
    public interface IMapReturnData
    {
        void selectedPosition(Position position);
    }
}
