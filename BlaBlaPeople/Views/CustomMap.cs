﻿using BlaBlaPeople.Models.Map;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Maps;
using static BlaBlaPeople.MapScreen;

namespace BlaBlaPeople.Views
{
    public class CustomMap : Map
    {
        public readonly List<MapPin> CustomPins = new List<MapPin>();
        public Position currentPosition;
        public State currentMapState;

        public void addPin(MapPin pin)
        {
            CustomPins.Add(pin);
            Pins.Add(pin);
        }

        public void showEventDetails(MapPin pin)
        {
            // TODO: open event details screen
            //Navigation.PushModalAsync(new EventDetailScreen(pin.PersonUsername));
        }

        public CustomMap() : base()
        {
        }

        public CustomMap(MapSpan region) : base(region)
        {
        }
    }
}
