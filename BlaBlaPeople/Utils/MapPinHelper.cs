﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;
using BlaBlaPeople.Models.Map;

namespace BlaBlaPeople.Utils.Map
{
    public class MapPinHelper
    {
        private static string tableName = "MapPins";
        public static async Task<List<MapPin>> getAll()
        {
            var pins = await FirebaseHelper.GetAllFunc<MapPin>(tableName);
            foreach (var pin in pins)
            {
                pin.Label = pin.Title;
                pin.Type = PinType.Place;
                pin.Position = new Position(pin.Latitude, pin.Longitude);
            }
            return pins;
        }

        public static async Task<MapPin> getByLocation(double latitude, double longitude)
        {
            var pins = await getAll();
            if (pins.Count > 0)
            {
                return (from pin in pins
                        where pin.Latitude == latitude && pin.Longitude == longitude
                        select pin).First();
            }
            // return null to prevent presenting pins with no data and default location
            return null;
        }

        public static async Task<bool> removeAll()
        {
            return await FirebaseHelper.DeleteAllData(tableName);
        }

        public static async Task<bool> remove(MapPin pin)
        {
            return await FirebaseHelper.DeleteFunc(tableName, pin);
        }

        public static async Task<bool> store(MapPin pin)
        {
            return await FirebaseHelper.AddFunc(tableName, pin);
        }
    }
}
