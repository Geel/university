﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BlaBlaPeople.Models
{
    public class User
    {
        public string Username { get; set; }
        public string Birthday { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public static User current { get; set; }

        public User(string use, string birth, string em, string pass)
        {
            Username = use;
            Birthday = birth;
            Email = em;
            Password = pass;
        }

        public User()
        {
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                User p = (User)obj;
                return Username.Equals(p.Username)
                    && Birthday.Equals(p.Birthday)
                    && Password.Equals(p.Password)
                    && Email.Equals(p.Email);
            }
        }

        public static async void Update_user(string Name, string uname, string bday, string mail)
        {
            var personlist = await FirebaseHelper.GetAllFunc<User>("Users");
            var founded = personlist.Find(a => a.Username == Name);
            var updated = (User)founded.Clone();
            updated.Username = uname;
            updated.Birthday = bday;
            updated.Email = mail;
            await FirebaseHelper.UpdateFunc<User>("Users", founded, updated);
        }

        public static async void Delete_user(string Name)
        {
            var personlist = await FirebaseHelper.GetAllFunc<User>("Users");
            var founded = personlist.Find(a => a.Username == Name);
            await FirebaseHelper.DeleteFunc<User>("Users", founded);
        }

        public static async Task<User> Get_user(string Name)
        {
            var personlist = await FirebaseHelper.GetAllFunc<User>("Users");
            var founded = personlist.Find(a => a.Username == Name);
            return founded;
        }
    }
}
