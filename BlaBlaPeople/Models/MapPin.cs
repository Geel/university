﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Maps;

namespace BlaBlaPeople.Models.Map
{
    public class MapPin : Pin
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string PersonUsername { get; set; }

        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                MapPin p = (MapPin)obj;
                return Title.Equals(p.Title)
                    && Description.Equals(p.Description)
                    && ImageUrl.Equals(p.ImageUrl)
                    && Latitude.Equals(p.Latitude)
                    && Longitude.Equals(p.Longitude)
                    && PersonUsername.Equals(p.PersonUsername);
            }
        }
    }
}
