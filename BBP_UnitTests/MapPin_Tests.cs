﻿using BlaBlaPeople.Models.Map;
using BlaBlaPeople.Utils.Map;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace BBP_UnitTests
{
    [TestClass]
    public class MapPin_Tests
    {
        [TestMethod]
        // There is an error "System.InvalidOperationException: You must call Xamarin.Forms.Forms.Init(); prior to using this property."
        // when MapPin constructor is executed so this test will be enabled after fixing the error
        [Ignore]
        // it is unable to test store without executing get and delete so will write one test who executes all functions
        public void Test_store_get_remove()
        {
            var title = "title";
            var description = "description";
            var imageUrl = "image url";
            double latitude = 49.845389891074646;
            double longitude = 24.02613034675618;
            var pin = new MapPin
            {
                Title = title,
                Description = description,
                ImageUrl = imageUrl,
                Latitude = latitude,
                Longitude = longitude
            };
            Assert.IsTrue(MapPinHelper.store(pin).Result);

            var pin1 = MapPinHelper.getByLocation(latitude, longitude).Result;
            Assert.AreEqual(pin1.Latitude, latitude);
            Assert.AreEqual(pin1.Longitude, longitude);
            Assert.AreEqual(pin1, pin);

            Assert.IsTrue(MapPinHelper.remove(pin).Result);

            pin = MapPinHelper.getByLocation(latitude, longitude).Result;
            Assert.IsNull(pin);
        }
    }
}
