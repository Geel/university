﻿using System;
using BlaBlaPeople;
using BlaBlaPeople.Models;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BBP_UnitTests
{
    [TestClass]
    public class RegexTest
    {
        [TestMethod]
        public void Test_phone_number_validation_positive()
        {
            var input = new RegexMethod();
            
            bool res = input.isPhoneNumber("+380681113344");
            bool res2 = input.isPhoneNumber("0681113344");
            bool res3 = input.isPhoneNumber("132-123-1234");
            bool res4 = input.isPhoneNumber("+3(068)1234567");
            bool res5 = input.isPhoneNumber("0 111 222 333");
            bool res6 = input.isPhoneNumber("12345");

            Assert.IsTrue(res);
            Assert.IsTrue(res2);
            Assert.IsTrue(res3);
            Assert.IsTrue(res4);
            Assert.IsTrue(res5);
            Assert.IsTrue(res6);
        }

        [TestMethod]
        public void Test_phone_number_validation_negative()
        {
            var input = new RegexMethod();

            bool res = input.isPhoneNumber("123 2 2");
            bool res2 = input.isPhoneNumber("06811g3344");
            bool res3 = input.isPhoneNumber("123-4-56");
            bool res4 = input.isPhoneNumber("132-123-1234-");
            bool res5 = input.isPhoneNumber("+7(926)1234567(");
            bool res6 = input.isPhoneNumber("353_424_2424");
            bool res7 = input.isPhoneNumber("1234");

            Assert.IsFalse(res);
            Assert.IsFalse(res2);
            Assert.IsFalse(res3);
            Assert.IsFalse(res4);
            Assert.IsFalse(res5);
            Assert.IsFalse(res6);
            Assert.IsFalse(res7);
        }

        [TestMethod]
        public void Test_email_validation_positive()
        {
            var input = new RegexMethod();

            bool res2 = input.isEmail("123.1@mail.ua");
            bool res3 = input.isEmail("123_2@mail.ua");
            bool res4 = input.isEmail("123AndriY@gmail.com");

            Assert.IsTrue(res2);
            Assert.IsTrue(res3);
            Assert.IsTrue(res4);
        }

        [TestMethod]
        public void Test_email_validation_negative()
        {
            var input = new RegexMethod();

            bool res = input.isEmail("123-@mail.ua");
            bool res2 = input.isEmail("123.@mail.ua");
            bool res3 = input.isEmail("123_@mail.ua");
            bool res4 = input.isEmail(".abc@mail.ua");
            bool res5 = input.isEmail("abc#de@mail.ua");
            bool res6 = input.isEmail("123And..riY@mail.ua");

            bool res7 = input.isEmail("abcde123@mail");
            bool res8 = input.isEmail("abcde123@gmail..ua");
            bool res9 = input.isEmail("abcde123@gmail.aa.bb.cc.u");
            bool res10 = input.isEmail("abcde123@gmail#mail.ua");

            Assert.IsFalse(res);
            Assert.IsFalse(res2);
            Assert.IsFalse(res3);
            Assert.IsFalse(res4);
            Assert.IsFalse(res5);
            Assert.IsFalse(res6);
            Assert.IsFalse(res7);
            Assert.IsFalse(res8);
            Assert.IsFalse(res9);
            Assert.IsFalse(res10);
        }

        // This test is excluded because it fails
        // Will be fixed in https://dhubatenko.atlassian.net/browse/BLAB-62
        [TestMethod]
        [Ignore]
        public void Test_email_validation_positive2()
        {
            var input = new RegexMethod();

            bool res = input.isEmail("123-A@mail.ua");
            bool res5 = input.isEmail("123AndriY@gmail.aa.bb.cc.ua");
            bool res6 = input.isEmail("123AndriY@gmail-mail.ua");

            Assert.IsTrue(res);
            Assert.IsTrue(res5);
            Assert.IsTrue(res6);
        }
    }

    [TestClass]
    public class CRUD_test
    {
        [TestMethod]
        public void Test_Add()
        {
            var pers = new User() { Username = "Jerry", Birthday = "20.02.2002", Email = "Thompson@", Password = "53" };
            var _ =  FirebaseHelper.AddFunc<User>("Users", pers).Result;
            var temp = User.Get_user("Jerry").Result;
            Assert.AreEqual(pers, temp);
            var clean = FirebaseHelper.DeleteFunc<User>("Users", pers).Result;
        }
        [TestMethod]
        public void Test_Update()
        {
            var pers = new User() { Username = "Jerry", Birthday = "20.02.2002", Email = "Thompson@", Password = "53" };
            var temp_ = FirebaseHelper.AddFunc<User>("Users", pers).Result;
            var updated = (User)pers.Clone();
            updated.Password = "20";
            var _ = FirebaseHelper.UpdateFunc<User>("Users", pers, updated).Result;
            var temp = User.Get_user("Jerry").Result;
            Assert.AreNotEqual(pers, temp);
            var clean1 = FirebaseHelper.DeleteFunc<User>("Users", updated).Result;
        }
        [TestMethod]
        public void Test_Delete()
        {
            var pers = new User() { Username = "Jerry", Birthday = "20.02.2002", Email = "Thompson@", Password = "53" };
            var _ = FirebaseHelper.DeleteFunc<User>("Users", pers).Result;
            var temp = User.Get_user("Jerry").Result;
            Assert.AreNotEqual(pers, temp);
        }
        [TestMethod]
        public void Test_GetAll()
        {
            var empt_lst = new List<User>();
            var temp = FirebaseHelper.GetAllFunc<User>("Users").Result;
            Assert.AreNotEqual(empt_lst, temp);
        }
    }
}
